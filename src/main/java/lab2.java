
final class Vector{
        protected final Double X;
        protected final Double Y;
        protected final Double Z;
        Vector(final Double x, final Double y, final Double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    public Double lengthOfVector()
    {
        return Math.sqrt(Math.pow(X,2)+Math.pow(Y,2)+Math.pow(Z,2));
    }
    public Double lengthOfVector(Double x, Double y, Double z)
    {
        return Math.sqrt(Math.pow(x,2)+Math.pow(y,2)+Math.pow(z,2));
    }
    public Double scalarProduct()
    {
        return Math.pow(lengthOfVector(),2);//Это если имелось, что скал.произв. на самого себя
    }
    public Double scalarProduct(Double x, Double y, Double z)
    {
        return X*x + Y*y + Z*z;
    }
    public Vector vectorProduct(Double x, Double y, Double z)
    {
        return new Vector(X*z-Z*y, Z*x-X*z, X*y-Y*x);
    }
    public Double cosinus(Double x1, Double y1, Double z1)
    {
        return scalarProduct(x1, y1, z1)/(lengthOfVector()*lengthOfVector(x1,y1,z1));
    }
    public Vector sum(Double x, Double y, Double z)
    {
        return new Vector(X+x,Y+y,Z+z);
    }
    public Vector razn(Double x, Double y, Double z)
    {
        return new Vector(X-x,Y-y,Z-z);
    }
    public static Vector[] generateVectors(Integer N)
    {
        Vector[] result = new Vector[N];
        while(N!=0)
        {
            result[N-1]=new Vector((Double)(Math.random()*100),(Double)(Math.random()*100),(Double)(Math.random()*100));
            N--;
        }
        return result;
    }
    public String toString()
    {
        return "{"+X+", "+Y+", "+Z+"}";
    }
    public String toString(Vector[] mas)
    {
        String result="";
        for(Vector i:mas)
            result+=i.toString()+" ";
        return result;
    }
}

public class lab2 {
    public static void main(String[] args)  {
        Vector test = new Vector(1.1, 2.2, 3.3);
        System.out.println("Длина вектора: "+test.lengthOfVector());
        System.out.println("Длина конкретного вектора: "+test.lengthOfVector(1.0,2.0,3.0));
        System.out.println("Скалярное произведение на себя: "+test.scalarProduct());
        System.out.println("Скалярное произведение на другой вектор: "+test.scalarProduct(1.0,2.4,3.1));
        System.out.println("Вектоное произведение: "+test.vectorProduct(1.0,2.4,3.1).toString());
        System.out.println("Косинус угла: "+test.cosinus(1.0,2.4,3.1));
        System.out.println("Сумма векторов: "+ test.sum(1.0,2.4,3.1).toString());
        System.out.println("Разность векторов: "+test.razn(1.0,2.4,3.1).toString());
        System.out.println("Случайные вектора: "+test.toString(Vector.generateVectors(5)));
    }
}


